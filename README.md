
# Build dossiers, V4 #

Cetain sources are considered to have "definitive" termination dates:

    Cabinet Members
    CarterTerritorialLeadership
    DAC
    Diplomats and Consuls
    Federal Judicial Center Database
    Postmasters
    US Attorneys
    US Marshals History
    USLHHS Database
    USLHHSRegister

Run:

    prep_data_for_build.py

    check_source_priority.ipynb

    build_dossiers.py
    
    retemplate_everything.py

    template_list_of_keys.py



# Build dossiers, V3 #

See contents of from_peter_102322/Dossier_Building/2022.10/

I'm now running **good_infer_continuous_service.py**.

**json_to_sqlite3.py** reads all_jobs.json and writes sqlite3 database, like always, although now the output folder is dated.

**copy_status.py** copies status from one database to another.  Didn't run it this time . . . 


# Build dossiers, V2 #

From Peter: 

        I’ve placed two new files in the “2022.09” subfolder of the “Dossier Building” folder.  “CivilOfficials.xlsx” combined all the data on individual appointments under a uniform row of fields.  “State Department Employees.xlsx” is a separate file that lists every individual State Department employee, but does not list their individual appointments.  The Government Employee Number links them to the data in CivilOfficials.

There are no command-line parameters for these scripts.  See the main of each.

**infer_continuous_service.py** reads from_peter081022/Dossier_Building/*.xslx.  **ssconvert** used to convert \*.xlsx to \*.csv.  The script infers continuous span of service in job for the people in Peter's data.  It writes a set of json files, the most important of which are **all_jobs.json** and **all_jobs_by_gen.json**, which gets fed into the later processeses.



# Build dossiers, V1 #

Delete Sheet1 from from_peter081022/Dossier_Building/Lighthouses.xlsx before processing.  But we don't process Lighthouses.xlsx because it doesn't have GEN's?  Eventually Peter will provide them.

There are no command-line parameters for these scripts.  See the main of each.

**infer_continuous_service.py** reads from_peter081022/Dossier_Building/*.xslx.  **ssconvert** used to convert \*.xlsx to \*.csv.  The script infers continuous span of service in job for the people in Peter's data.  It writes a set of json files, the most important of which are **all_jobs.json** and **all_jobs_by_gen.json**, which gets fed into the later processeses.

**json_to_sqlite3.py** reads all_jobs.json and writes sqlite3 database.



