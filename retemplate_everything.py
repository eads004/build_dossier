#!/home/spenteco/anaconda2/envs/py3/bin/python

import glob, subprocess, json, re
from datetime import datetime
import pandas as pd
from mako.template import Template

if __name__ == '__main__':
    
    all_jobs = json.load(open('UPDATED.all_jobs.json', 'r', encoding='utf-8'))
    
    # ------------------------------------------------------------------
    
    now = datetime.now().strftime("%Y/%m/%d, %H:%M:%S")
    file_now = re.sub('[^0-9]', '_', now)
    
    template = Template(filename='report_template.html')
    
    for a in range(0, 6):
        
        split_all_jobs = all_jobs[a * 2650: (a * 2650) + 2650]
    
        html = template.render(now=now, all_jobs=split_all_jobs)
        
        f = open('kastor_proof_reports_no_subloc_010323/test_010323.' + str(a) + '.html', 'w', encoding='utf-8')
        f.write(html)
        f.close()
    
    # ------------------------------------------------------------------
