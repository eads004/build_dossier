#!/home/spenteco/anaconda2/envs/py3/bin/python

import sqlite3
    
if __name__ == '__main__':

    OLD_DATABASE = '/home/spenteco/3/build_dossier/sqlite3_data_091522/jobs_data.sqlite3'

    NEW_DATABASE = '/home/spenteco/3/build_dossier/sqlite3_data_102322/jobs_data.sqlite3'

    OLD_connection = sqlite3.connect(OLD_DATABASE)

    OLD_cursor = OLD_connection.cursor()

    NEW_connection = sqlite3.connect(NEW_DATABASE)

    NEW_cursor = NEW_connection.cursor()

    gens = NEW_cursor.execute('select GEN from gens;').fetchall()
    
    for g in gens:

        old_status = OLD_cursor.execute('select GEN, status FROM gens WHERE GEN = "' + g[0] + '";').fetchall()[0][1]

        NEW_cursor.execute('UPDATE gens SET status = "' + old_status + '" WHERE GEN = "' + g[0] + '";')

    NEW_connection.commit()

    print('ok')
