.mode csv
.import details.csv details
.import jobs.csv jobs
.import gens.csv gens
.import original_data_StateDepartmentEmployees.csv original_data_StateDepartmentEmployees
.import end_years.csv end_years
.import original_data_CivilOfficials.csv original_data_CivilOfficials