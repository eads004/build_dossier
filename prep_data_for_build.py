#!/home/spenteco/anaconda2/envs/py3/bin/python

import glob, subprocess, json
import pandas as pd

def convert_input_to_csv(INPUT_FOLDER):
    
    for p in glob.glob(INPUT_FOLDER + '*.xlsx'):

        #if 'CivilOfficials' in p:
        if 'CivilOfficials2023.03.01.xlsx' in p:
        
            cmd = 'ssconvert ' + p + ' ' + p.replace('.xlsx', '.csv')
            
            result = subprocess.getoutput(cmd)
        
    csv_files = [p.split('/')[-1] for p in glob.glob(INPUT_FOLDER + '*.csv')]
    
    expected_files = ['CivilOfficials2023.03.01.csv',]
                            
    for f in csv_files:
        if f not in expected_files:
            print()
            print('ERROR: file', f, 'not expected')
                            
    for f in expected_files:
        if f not in csv_files:
            print()
            print('ERROR: file', f, 'is missing')
    
    return csv_files
    
def fix_location(location):

    loc_parts = [p for p in location.split(',') if p > '']
    return ','.join(loc_parts)
    
def load_and_standardize_inputs(INPUT_FOLDER, csv_files):
    
    original_records = []
    standardized_records = []
    
    print('csv_files', csv_files, 'INPUT_FOLDER', INPUT_FOLDER)
    
    for f in csv_files:
        
        #if f in ['CivilOfficials.csv', 
        #            'JudicialAppointments.csv',
        #            'StateDepartmentAppointments.csv',
        #            'Postmasters.csv',
        #            'StateDepartmentEmployees.csv']:
        
        #if f in ['NEW.CivilOfficials.csv']:
        if f in ['CivilOfficials2023.03.01.csv']:
                        
            print('LOADING', f)
            
            k = 'GovernmentEmployeeNumber'
            if f == 'Postmasters.csv':
                k = 'Government Employee Number'
                
            for i, r in enumerate(pd.read_csv(INPUT_FOLDER + f, 
                                        dtype={'OK?': str,
                                                'CONCURRENT?': str,
                                                'Source information': str,
                                                'RowNumber': str,
                                                'Data Set': str,
                                                'id': str,
                                                'GovernmentEmployeeNumber': str,
                                                'Source ID': str,
                                                'SourceCategory': str,
                                                'Volume': str,
                                                'Page': str,
                                                'Image': str,
                                                'Record Year': str,
                                                'Person Information': str,
                                                'Full Name': str,
                                                'Title': str,
                                                'First Name': str,
                                                'Middle Name': str,
                                                'Last Name': str,
                                                'Suffix': str,
                                                'Individual Name': str,
                                                'Individual Title': str,
                                                'Individual First': str,
                                                'Individual Middle': str,
                                                'Individual Last': str,
                                                'Individual Suffix': str,
                                                'Birth Year': str,
                                                'Death Year': str,
                                                'Birth Locality': str,
                                                'Birth State': str,
                                                'Birth Country': str,
                                                'Birth Sector': str,
                                                'Birth Polity': str,
                                                'Locality of Residence': str,
                                                'State of Residence': str,
                                                'Country of Residence': str,
                                                'Sector of Residence': str,
                                                'Polity of Residence': str,
                                                'Nationality': str,
                                                'Polity of Nationality': str,
                                                'Member of Congress': str,
                                                'Appointment Information': str,
                                                'WHO IS IT?': str,
                                                'Office': str,
                                                'Section': str,
                                                'Division': str,
                                                'Department': str,
                                                'Occupation Type': str,
                                                'Occupation Status': str,
                                                'Vessel': str,
                                                'Geocoding Location': str,
                                                'Building': str,
                                                'Sub-Locality': str,
                                                'Locality': str,
                                                'State Section': str,
                                                'State': str,
                                                'Region': str,
                                                'Native Nation': str,
                                                'Country': str,
                                                'Sector': str,
                                                'Polity': str,
                                                'Start Year': str,
                                                'Start Month': str,
                                                'Start Day': str,
                                                'End Year': str,
                                                'End Month': str,
                                                'End Day': str,
                                                'Departure Reason': str,
                                                'Earliest Year': str,
                                                'Earliest Month': str,
                                                'Earliest Day': str,
                                                'Latest Year': str,
                                                'Latest Month': str,
                                                'Latest Day': str,
                                                'About the Office': str,
                                                'YearPostCreated': str,
                                                'MonthPostCreated': str,
                                                'DayPostCreated': str,
                                                'Creation Modifier': str,
                                                'YearPostTerminated': str,
                                                'MonthPostTerminated': str,
                                                'DayPostTerminated': str,
                                                'Termination Modifier': str,
                                                'Compensation': str,
                                                'Start Salary': str,
                                                'End Salary': str,
                                                'Salary': str,
                                                'Pay Period': str,
                                                'Rations': str,
                                                'Per Diem': str,
                                                'Emoluments': str,
                                                'Nett Compensation': str,
                                                'Expenses': str,
                                                'Commissions': str,
                                                'Appointment Information': str,
                                                'president': str,
                                                'nom_month': str,
                                                'nom_day': str,
                                                'nom_year': str,
                                                'confirm_month': str,
                                                'confirm_day': str,
                                                'confirm_year': str,
                                                'confirm_votes_for': str,
                                                'confirm_votes_against': str,
                                                'Confirmed (Y/N)': str,
                                                'commission_month': str,
                                                'commission_day': str,
                                                'commission_year': str,
                                                'termination_month': str,
                                                'termination_day': str,
                                                'termination_year': str,
                                                'appointee_departure_reason': str,
                                                'predecessor_departure_reason': str,
                                                'Additional Information': str,
                                                'Notes': str,
                                                'OfficeRECORD': str,
                                                'SectionRECORD': str,
                                                'DivisionRECORD': str,
                                                'RECORD': str
                                        }) \
                                        .fillna('').to_dict('records')):
                
                try:
                    if ''.join(r.values()).strip() == '':
                        continue
                except:
                    pass
                    
                if r['Confirmed (Y/N)'] == 'N':
                    print('Dropping', r['GovernmentEmployeeNumber'])
                    continue
                
                original_records.append(r)
                
                standard_row = {}
                
                #if f == 'NEW.CivilOfficials.csv':
                if f == 'CivilOfficials2023.03.01.csv':

                    occupation_status = r['Occupation Status']
                    occupation_type = r['Occupation Type']

                    if occupation_status.strip() == '' and \
                        occupation_type.strip() == '' and \
                        r['SourceCategory'].strip() == 'Cabinet Members':

                        occupation_type = r['Office']
                        occupation_status = 'Leadership'
                                
                    real_end_year = str(r['termination_year']).split('.')[0]
                    if real_end_year.strip() == '':
                        real_end_year = str(r['End Year']).split('.')[0]
                                
                    real_record_year = str(r['Record Year']).split('.')[0]
                    if real_record_year.strip() in ['', '0.0']:
                        real_record_year = str(r['Start Year']).split('.')[0]

                    location = fix_location(str(r['State Section']) + ',' + \
                                                            str(r['Locality']))

                    real_source_category = '9 ' + r['SourceCategory']
                        
                    if r['SourceCategory'] in ['SEJ', 'Registers']:
                        real_source_category = '2 ' + r['SourceCategory']
                    else:
                        real_source_category = '1 ' + r['SourceCategory']

                    if real_source_category not in ['1 Postmasters'] and 'lighthouse' not in r['Office'].lower():

                        standard_row = {    'GEN': r['GovernmentEmployeeNumber'],
                                            'name': r['Full Name'],
                                            'office': r['Office'], 
                                            'occupation_status': occupation_status, 
                                            'occupation_type': occupation_type,
                                            'state': r['State'],
                                            'location': location,
                                            'start_year': str(r['Start Year']).split('.')[0],
                                            'end_year': real_end_year.split('.')[0],
                                            'section': str(r['Section']),
                                            'division': str(r['Division']),
                                            'department': str(r['Department']),
                                            'source_category': real_source_category,
                                            'record_year': str(real_record_year).split('.')[0],
                                            'input_file': f,
                                            'original_record': json.dumps(r),
                                            'concurrent?': r['CONCURRENT?'],
                                            'confirmed?': r['Confirmed (Y/N)'],
                                            'okay?': r['OK?']
                                        }

                        standardized_records.append(standard_row)
 
    for ai, a in enumerate(standardized_records):
        if a['name'].strip() == '':
            for b in standardized_records:
                if a['GEN'] == b['GEN'] and b['name'].strip() > '':
                    standardized_records[ai]['name'] = b['name'].strip()
                    break
    
    df = pd.DataFrame(standardized_records)
    
    print('df.shape', df.shape);
    
    return df, original_records, standardized_records

def get_register_dates(df):

    register_dates = df.groupby(['source_category', 'record_year']) \
                                .size() \
                                .reset_index() \
                                .rename(columns={0:'count'}).to_dict('records')

    register_dates = [str(a['record_year']).split('.')[0] for a in register_dates \
                        if a['source_category'] == '4 Registers'] + ["1834"]
    
    return register_dates
    
def get_unique_gens(df):

    unique_gens = df.groupby(['GEN',]) \
                                .size() \
                                .reset_index() \
                                .rename(columns={0:'count'})

    unique_gens = unique_gens.to_dict('records')
    
    return unique_gens

def dump_to_json(data, file_name):
    
    f = open(file_name, 'w', encoding='utf-8')
    f.write(json.dumps(data, indent=4))
    f.close()


if __name__ == '__main__':

    INPUT_FOLDER = 'from_peter_031323/2023.03/'
    
    # ------------------------------------------------------------------
    
    csv_files = convert_input_to_csv(INPUT_FOLDER)

    df, original_records, standardized_records = \
            load_and_standardize_inputs(INPUT_FOLDER, csv_files)
    
    register_dates = get_register_dates(df)
    
    unique_gens = get_unique_gens(df)
    
    # ------------------------------------------------------------------
    
    df.to_csv('standardized_records.csv', index=False)
    
    dump_to_json(original_records, 'original_records.json')
    
    dump_to_json(register_dates, 'register_dates.json')
    
    dump_to_json(unique_gens, 'unique_gens.json')
    
    # ------------------------------------------------------------------
