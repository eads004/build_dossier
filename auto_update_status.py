#!/home/spenteco/anaconda2/envs/py3/bin/python

import sqlite3
    
if __name__ == '__main__':

    DATABASE = '/home/spenteco/3/build_dossier/sqlite3_data_102322/jobs_data.sqlite3'

    connection = sqlite3.connect(DATABASE)

    cursor = connection.cursor()

    gens = cursor.execute('select GEN, count(*) as N FROM details GROUP BY 1 HAVING N = 1').fetchall()
    
    for g in gens:

        cursor.execute('UPDATE gens SET status = "AUTO" WHERE GEN = "' + g[0] + '";')

    connection.commit()

    print('ok')
