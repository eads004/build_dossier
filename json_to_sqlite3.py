#!/home/spenteco/anaconda2/envs/py3/bin/python
    
import json, subprocess, copy, glob
import pandas as pd

def write_csv(data, file_name, OUTPUT_FOLDER):

    df = pd.DataFrame(data)
    df.to_csv(OUTPUT_FOLDER + file_name, index=False)


def json_to_csv_files(INPUT_FILE, OUTPUT_FOLDER, SELECTED_GENS):

    all_jobs = json.load(open(INPUT_FILE, 'r', encoding='utf-8'))
    
    all_gens = []
    
    gens_csv = []
    jobs_csv = []
    details_csv = []
    source_records = {}
    end_years_csv = []
    
    for job_n, job in enumerate(all_jobs):
    
        this_job = copy.deepcopy(job)
    
        details = copy.deepcopy(this_job['details'])
        del this_job['details']
        this_job['job_n'] = job_n

        if SELECTED_GENS != None and this_job['GEN'] not in SELECTED_GENS:
            continue
        
        all_gens.append(this_job['GEN'])
        
        jobs_csv.append(this_job)
        
        for detail_n, detail in enumerate(details):
        
            this_detail = copy.deepcopy(detail)
            
            input_file = copy.deepcopy(this_detail['input_file'])
            original_record = json.loads(this_detail['original_record'])
            
            original_record['GEN'] =  this_job['GEN']
            original_record['job_n'] =  job_n
            original_record['detail_n'] =  detail_n
            
            refined_possible_end_years = copy.deepcopy(this_detail['refined_possible_end_years'])
            
            del this_detail['input_file']
            del this_detail['original_record']
            del this_detail['refined_possible_end_years']
            
            this_detail['job_n'] = job_n
            this_detail['detail_n'] = detail_n
        
            details_csv.append(this_detail)
        
            if input_file not in source_records:
                source_records[input_file] = []
            source_records[input_file].append(original_record)
        
            for y_n, y in enumerate(refined_possible_end_years):
            
                possible_end_year = {'GEN': this_job['GEN'],
                                        'job_n': job_n,
                                        'detail_n': detail_n,
                                        'sequence': y_n, 
                                        'year': y[0], 
                                        'reason': y[1]}
                
                end_years_csv.append(possible_end_year)
                
    
    for g in sorted(list(set(all_gens))):
        gens_csv.append({'GEN': g, 'status': 'not_checked'})
    
    write_csv(gens_csv, 'gens.csv', OUTPUT_FOLDER)
    write_csv(jobs_csv, 'jobs.csv', OUTPUT_FOLDER)
    write_csv(details_csv, 'details.csv', OUTPUT_FOLDER)
    write_csv(end_years_csv, 'end_years.csv', OUTPUT_FOLDER)
    
    for k, v in source_records.items():
        write_csv(v, 'original_data_' + k, OUTPUT_FOLDER)
    
    
def csv_to_sqlite(OUTPUT_FOLDER):

    all_sqlite_commands = ['.mode csv']

    for p in glob.glob(OUTPUT_FOLDER + '*.csv'):
        
        all_sqlite_commands.append('.import ' + p.split('/')[-1] + ' ' + \
                                                p.split('/')[-1].split('.')[0])
                                                
    f = open(OUTPUT_FOLDER + 'load_database.sql', 'w')
    f.write('\n'.join(all_sqlite_commands))
    f.close()
    
    cmd = 'cd ' + OUTPUT_FOLDER + '; sqlite3  jobs_data.sqlite3 < load_database.sql'
    
    print(cmd)
    
    print(subprocess.getoutput(cmd))
    
if __name__ == '__main__':

    INPUT_FILE = 'all_jobs.json'
    OUTPUT_FOLDER = 'sqlite3_data_102322/'

    SELECTED_GENS = None
    #SELECTED_GENS = ['GEN.000001', 'GEN.000002', 'GEN.000006', 'GEN.000007', 'GEN.000026']
    
    subprocess.getoutput('rm -r ' + OUTPUT_FOLDER + '*')
    
    json_to_csv_files(INPUT_FILE, OUTPUT_FOLDER, SELECTED_GENS)
    
    csv_to_sqlite(OUTPUT_FOLDER)
    
    subprocess.getoutput('chmod -R 777 ' + OUTPUT_FOLDER)
    
