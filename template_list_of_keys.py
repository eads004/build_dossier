#!/home/spenteco/anaconda2/envs/py3/bin/python

import glob, subprocess, json, re
from datetime import datetime
import pandas as pd
from mako.template import Template


if __name__ == '__main__':

    JOB_FILE = 'UPDATED.all_jobs.json'

    #INPUT_FILE = '84_gen.txt'
    #DESIRED_TYPE = 'n_not_counted'

    #INPUT_FILE = '459_gen.txt'
    #DESIRED_TYPE = 'more_than_one_end_date_in_data'

    INPUT_FILE = 'not_confirmed.txt'
    DESIRED_TYPE = ''
    
    # ------------------------------------------------------------------

    SELECTED_GENS = set([t.strip() for t in open(INPUT_FILE).read().split('\n') if t.strip() > ''])
    
    print('len(SELECTED_GENS)', len(SELECTED_GENS))
    
    all_jobs = json.load(open(JOB_FILE, 'r', encoding='utf-8'))
    
    selected_jobs = [person for person in all_jobs if person['GEN'] in SELECTED_GENS]
    
    for person_n, person in enumerate(selected_jobs):
        for job_n, job in enumerate(person['jobs']):
            if selected_jobs[person_n]['jobs'][job_n]['end_date_type'].startswith(DESIRED_TYPE) == False:
            
                selected_jobs[person_n]['jobs'][job_n]['end_date_type'] = ''
                
            for detail_n, detail in enumerate(job['job_details']):
                
                if selected_jobs[person_n]['jobs'][job_n]['job_details'][detail_n]['confirmed?'] == 'N':
                    selected_jobs[person_n]['jobs'][job_n]['job_details'][detail_n]['confirmed?'] = '**** NOT CONFIRMED ****'
                else:
                    selected_jobs[person_n]['jobs'][job_n]['job_details'][detail_n]['confirmed?'] = ''
    
    print('len(selected_jobs)', len(selected_jobs))
    
    # ------------------------------------------------------------------
    
    template = Template(filename='SPECIAL.report_template.html')
    
    now = datetime.now().strftime("%Y/%m/%d, %H:%M:%S")
    
    html = template.render(now=now, all_jobs=selected_jobs)
        
    f = open('kastor_proof_reports_no_subloc_010323/' + \
                INPUT_FILE.replace('.txt', '.html'), 'w', encoding='utf-8')
    f.write(html)
    f.close()
    
    # ------------------------------------------------------------------
