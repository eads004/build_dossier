#!/home/spenteco/anaconda2/envs/py3/bin/python

import glob, subprocess, json
import pandas as pd

#
#   PREP STUFF
#

def convert_input_to_csv(INPUT_FOLDER):
    
    for p in glob.glob(INPUT_FOLDER + '*.xlsx'):
        
        cmd = 'ssconvert ' + p + ' ' + p.replace('.xlsx', '.csv')
        
        result = subprocess.getoutput(cmd)
        
    csv_files = [p.split('/')[-1] for p in glob.glob(INPUT_FOLDER + '*.csv')]
    
    expected_files = ['CivilOfficials.csv', 'StateDepartmentEmployees.csv']
                            
    for f in csv_files:
        if f not in expected_files:
            print()
            print('ERROR: file', f, 'not expected')
                            
    for f in expected_files:
        if f not in csv_files:
            print()
            print('ERROR: file', f, 'is missing')
    
    return csv_files
    
def fix_location(location):

    loc_parts = [p for p in location.split(',') if p > '']
    return ','.join(loc_parts)
    
def load_and_standardize_inputs(INPUT_FOLDER, csv_files):
    
    original_records = []
    standardized_records = []
    
    for f in csv_files:
        
        #if f in ['CivilOfficials.csv', 
        #            'JudicialAppointments.csv',
        #            'StateDepartmentAppointments.csv',
        #            'Postmasters.csv',
        #            'StateDepartmentEmployees.csv']:
        
        if f in ['CivilOfficials.csv',
                    'StateDepartmentEmployees.csv']:
                        
            print('LOADING', f)
            
            k = 'GovernmentEmployeeNumber'
            if f == 'Postmasters.csv':
                k = 'Government Employee Number'
                
            for i, r in enumerate(pd.read_csv(INPUT_FOLDER + f).fillna('').to_dict('records')):
                
                try:
                    if ''.join(r.values()).strip() == '':
                        continue
                except:
                    pass
                
                original_records.append(r)
                
                standard_row = {}
                
                if f == 'CivilOfficials.csv':
                    standard_row = {    'GEN': r['GovernmentEmployeeNumber'],
                                        'name': r['Full Name'],
                                        'office': r['Office'], 
                                        'occupation_status': r['Occupation Status'], 
                                        'occupation_type': r['Occupation Type'],
                                        'state': r['State'],
                                        'location': fix_location(str(r['State Section']) + ',' + \
                                                            str(r['Locality']) + ',' + \
                                                            str(r['Sub-Locality'])),
                                        'start_year': str(r['Record Year']).split('.')[0],
                                        'end_year': str(r['termination_year']).split('.')[0],
                                        'section': str(r['Section']),
                                        'division': str(r['Division']),
                                        'department': str(r['Department']),
                                        'source_category': r['SourceCategory'],
                                        'record_year': str(r['Record Year']).split('.')[0],
                                        'input_file': f,
                                        'original_record': json.dumps(r)
                                    }
                
                if f == 'JudicialAppointments.csv':
                    
                    name = r['First']
                    if r['Middle'].strip() > '':
                        name = name + ' ' + r['Middle']
                    name = name + ' ' + r['Last']
                    if r['Suffix'].strip() > '':
                        name = name + ' ' + r['Suffix']
                    
                    standard_row = {    'GEN': r['GovernmentEmployeeNumber'],
                                        'name': name,
                                        'office': r['Office'] + ',' + r['Division'], 
                                        'occupation_status': '', 
                                        'occupation_type': 'Judicial Appointment',
                                        'state': r['State'],
                                        'location': fix_location(r['State Section'] + ',' + \
                                                        r['Locality']),
                                        'start_year': str(r['StartYear']),
                                        'end_year': str(r['EndYear']),
                                        'section': str(r['Section']),
                                        'division': str(r['Division']),
                                        'department': str(r['Department']),
                                        'source_category': r['SourceCategory'],
                                        'record_year': '',
                                        'input_file': f,
                                        'original_record': json.dumps(r)
                                    }
                
                if f == 'Postmasters.csv':
                    
                    name = r['Postmaster First Name']
                    if r['Postmaster Middle Initial/Name'].strip() > '':
                        name = name + ' ' + r['Postmaster Middle Initial/Name']
                    name = name + ' ' + r['Postmaster Last Name']
                    if r['Postmaster Suffix'].strip() > '':
                        name = name + ' ' + r['Postmaster Suffix']
                        
                    standard_row = {    'original_records_i': len(original_records) -1,
                                        'GEN': r['Government Employee Number'],
                                        'name': name,
                                        'office': 'Postmaster', 
                                        'occupation_status': '', 
                                        'occupation_type': 'Postmaster',
                                        'state': r['State'],
                                        'location': r['Locality'],
                                        'start_year': str(r['Yappointed']),
                                        'end_year': str(r['Yterminated']),
                                        'section': '',
                                        'division': '',
                                        'department': 'Postmaster General',
                                        'source_category': 'Postmasters',
                                        'record_year': '',
                                        'input_file': f,
                                        'original_record': json.dumps(r)
                                    }
                
                if f == 'StateDepartmentAppointments.csv':
                    
                    name = r['First Name']
                    if r['Middle Name'].strip() > '':
                        name = name + ' ' + r['Middle Name']
                    name = name + ' ' + r['Last Name']
                    if r['Suffix'].strip() > '':
                        name = name + ' ' + r['Suffix']
                        
                    standard_row = {    'GEN': r['GovernmentEmployeeNumber'],
                                        'name': name,
                                        'office': r['Office'], 
                                        'occupation_status': r['Occupation Status'], 
                                        'occupation_type': r['Occupation Type'],
                                        'state': r['State'],
                                        'location': fix_location(str(r['Country']) + ',' + \
                                                            str(r['Region']) + ',' + \
                                                            str(r['Locality'])),
                                        'start_year': str(r['Start Year']),
                                        'end_year': str(r['End Year']),
                                        'section': str(r['Section']),
                                        'division': str(r['Division']),
                                        'department': str(r['Department']),
                                        'source_category': r['SourceCategory'],
                                        'record_year': str(r['Record Year']).split('.')[0],
                                        'input_file': f,
                                        'original_record': json.dumps(r)
                                    }
                
                if f == 'StateDepartmentEmployees.csv':
                    standard_row = {    'GEN': r['GovernmentEmployeeNumber'],
                                        'name': r['Full Name'],
                                        'office': 'State Department Employee', 
                                        'occupation_status': '', 
                                        'occupation_type': 'State Department Employee',
                                        'state': str(r['Country of Residence']) + '.,' + \
                                                str(r['State of Residence']),
                                        'location': '',
                                        'start_year': '',
                                        'end_year': '',
                                        'section': '',
                                        'division': '',
                                        'department': 'State',
                                        'source_category': r['SourceCategory'],
                                        'record_year': '',
                                        'input_file': f,
                                        'original_record': json.dumps(r)
                                    }
                    
                standardized_records.append(standard_row)
            
        if f == 'Lighthouses.csv':
            print()
            print('NOTICE: Lighthouses.csv not loaded because it has no GEN values.')

    for ai, a in enumerate(standardized_records):
        if a['name'].strip() == '':
            for b in standardized_records:
                if a['GEN'] == b['GEN'] and b['name'].strip() > '':
                    standardized_records[ai]['name'] = b['name'].strip()
                    break
    
    df = pd.DataFrame(standardized_records)
    
    return df, original_records, standardized_records

def get_register_dates(df):

    register_dates = df.groupby(['source_category', 'record_year']) \
                                .size() \
                                .reset_index() \
                                .rename(columns={0:'count'}).to_dict('records')

    register_dates = [a['record_year'] for a in register_dates \
                        if a['source_category'] == 'Registers'] + ["1834"]
    
    return register_dates
    
def get_unique_gens(df):

    unique_gens = df.groupby(['GEN',]) \
                                .size() \
                                .reset_index() \
                                .rename(columns={0:'count'})

    unique_gens = unique_gens.to_dict('records')
    
    return unique_gens

#
#  ACTUAL PROCESS HERE
#

def ok_to_backfill(years):
    
    result = True
    for y in years:
        if y[1].startswith('another'):
            result = False
    
    return result
    
def cull_years(years):
 
    result = []
    for y in years:
        result.append(y)
        if result[-1][1].startswith('another'):
            break
    return result

def records_to_jobs(df, register_dates, unique_gens, SELECTED_GENS):
    
    all_jobs = []

    for gen_n, gen in enumerate(unique_gens):

        if SELECTED_GENS != None and gen['GEN'] not in SELECTED_GENS:
            continue
        
        records_for_gen = df.loc[df['GEN'] == gen['GEN']]

        records_for_gen = records_for_gen[['GEN', 'name', 
                     'source_category', 'record_year', 
                     'location', 'state',
                     'office', 'occupation_type', 'occupation_status', 
                     'section', 'division', 'department',
                     'start_year', 'end_year', 'input_file', 'original_record']].sort_values(by=['record_year'])

        records_for_gen = records_for_gen.to_dict('records')
        
        for a in range(0, len(records_for_gen)):

            possible_end_years = []

            for b in range(a, len(records_for_gen)):

                match_type = ''

                if records_for_gen[a]['location'] == records_for_gen[b]['location'] and \
                    records_for_gen[a]['state'] == records_for_gen[b]['state']:

                    if records_for_gen[a]['occupation_type'] == records_for_gen[b]['occupation_type'] and \
                        records_for_gen[a]['occupation_status'] == records_for_gen[b]['occupation_status']:

                        if b == a:
                            match_type = 'starting date (a)'
                        else:
                            match_type = 'same office type, status, and place (a)'
                    else:
                        a_office_words = records_for_gen[a]['office'].lower().split(' ')
                        b_office_words = records_for_gen[b]['office'].lower().split(' ')

                        office_word_tokens = a_office_words + b_office_words
                        office_word_types = set(a_office_words + b_office_words)

                        if float(len(office_word_tokens)) / float(len(office_word_types)) >= 1.5:
                            match_type = 'same office words (a)'
                        else:
                            break

                if match_type > '':
                    possible_end_years.append([records_for_gen[b]['record_year'], match_type])

            #print()
            #print(gen['GEN'], 'possible_end_years', possible_end_years)
                    
            #
            #    DID ANYONE ELSE HAVE THE JOB?
            #
                    
            refined_possible_end_years = []

            for b in range(0, len(possible_end_years)):

                refined_possible_end_years.append(possible_end_years[b])

                if b < len(possible_end_years) - 1:

                    from_year = possible_end_years[b][0]
                    to_year = possible_end_years[b + 1][0]

                    records_for_others = df.loc[df['record_year'] > from_year]
                    records_for_others = records_for_others.loc[records_for_others['record_year'] < to_year]

                    records_for_others = records_for_others.loc[records_for_others['GEN'] != \
                                                     records_for_gen[a]['GEN']]

                    records_for_others = records_for_others.loc[records_for_others['location'] == \
                                                     records_for_gen[a]['location']]
                    records_for_others = records_for_others.loc[records_for_others['state'] == \
                                                     records_for_gen[a]['state']]
                    records_for_others = records_for_others.loc[records_for_others['occupation_type'] == \
                                                     records_for_gen[a]['occupation_type']]
                    records_for_others = records_for_others.loc[records_for_others['occupation_status'] == \
                                                     records_for_gen[a]['occupation_status']]

                    records_for_others = records_for_others[['GEN', 'name', 
                                                     'source_category', 'record_year', 
                                                     'location', 'state',
                                                     'office', 'occupation_type', 'occupation_status', 
                                                     'start_year', 'end_year']] \
                                            .sort_values(by=['record_year']) \
                                            .to_dict('records')
                    
                    if len(records_for_others) > 0 and \
                        refined_possible_end_years[-1][1].startswith('another') == False:

                        a_office_words = records_for_gen[a]['office'].lower().split(' ')
                        rfo_office_words = records_for_others[0]['office'].lower().split(' ')

                        office_word_tokens = a_office_words + rfo_office_words
                        office_word_types = set(a_office_words + rfo_office_words)

                        if float(len(office_word_tokens)) / float(len(office_word_types)) >= 1.5:
                            
                            refined_possible_end_years.append([records_for_others[0]['record_year'], 
                                'another person same occupation etc (a) ' + records_for_others[0]['GEN']])
                    else:

                        records_for_others = df.loc[df['record_year'] > from_year]
                        records_for_others = records_for_others.loc[records_for_others['record_year'] < to_year]

                        records_for_others = records_for_others.loc[records_for_others['GEN'] != \
                                                         records_for_gen[a]['GEN']]

                        records_for_others = records_for_others.loc[records_for_others['location'] == \
                                                         records_for_gen[a]['location']]
                        records_for_others = records_for_others.loc[records_for_others['state'] == \
                                                         records_for_gen[a]['state']]

                        records_for_others = records_for_others[['GEN', 'name', 
                                                         'source_category', 'record_year', 
                                                         'location', 'state',
                                                         'office', 'occupation_type', 'occupation_status', 
                                                         'start_year', 'end_year']] \
                                                .sort_values(by=['record_year']) \
                                                .to_dict('records')
                        for rfo in records_for_others:

                            a_office_words = records_for_gen[a]['office'].lower().split(' ')
                            rfo_office_words = rfo['office'].lower().split(' ')

                            office_word_tokens = a_office_words + rfo_office_words
                            office_word_types = set(a_office_words + rfo_office_words)

                            if float(len(office_word_tokens)) / float(len(office_word_types)) >= 1.5:
                                refined_possible_end_years.append([
                                        records_for_others[0]['record_year'], 
                                        'another person, same office (a) ' + rfo['GEN']])
                                break

            #print(gen['GEN'], 'refined_possible_end_years (a)', refined_possible_end_years)
                                
            #
            #    LOOK AHEAD IN THE REGISTERS
            #       

            if refined_possible_end_years[-1][0] == records_for_gen[a]['record_year'] and \
                records_for_gen[a]['source_category'] == 'Registers' and \
                int(records_for_gen[a]['record_year']) >= 1816 and \
                refined_possible_end_years[-1][1].startswith('another') == False:
                    
                next_date = None
                for d in range(0, len(register_dates)):

                    if register_dates[d] == records_for_gen[a]['record_year'] and \
                        d < len(register_dates) - 1:

                        next_date = register_dates[d + 1]
                        break

                if next_date != None and refined_possible_end_years[-1][1].startswith('another') == False:

                    refined_possible_end_years.append([next_date, 'not in later register'])

                    if a > 0:
                        for b in range(0, a):
                            
                            if records_for_gen[b]['refined_possible_end_years'][-1][0] == \
                                        refined_possible_end_years[-2][0]:

                                match_type = ''

                                if records_for_gen[a]['location'] == records_for_gen[b]['location'] and \
                                    records_for_gen[a]['state'] == records_for_gen[b]['state']:

                                    if records_for_gen[a]['occupation_type'] == records_for_gen[b]['occupation_type'] and \
                                        records_for_gen[a]['occupation_status'] == records_for_gen[b]['occupation_status']:

                                            if b == a:
                                                match_type = 'starting date (b)'
                                            else:
                                                match_type = 'same office type, status, and place (b)'
                                                    
                                    else:
                                        a_office_words = records_for_gen[a]['office'].lower().split(' ')
                                        b_office_words = records_for_gen[b]['office'].lower().split(' ')

                                        office_word_tokens = a_office_words + b_office_words
                                        office_word_types = set(a_office_words + b_office_words)

                                        if float(len(office_word_tokens)) / float(len(office_word_types)) >= 1.5:
                                            
                                            match_type = 'same office words (b)'
                                        else:
                                            break
                                            
                                if match_type > '':
                                    
                                    if ok_to_backfill(records_for_gen[b]['refined_possible_end_years']):

                                        records_for_gen[b]['refined_possible_end_years'] \
                                                        .append([refined_possible_end_years[-1][0],
                                                                 refined_possible_end_years[-1][1] + ' BKFL'])

            #print(gen['GEN'], 'refined_possible_end_years (b)', refined_possible_end_years)
                                
            refined_possible_end_years = cull_years(refined_possible_end_years)
    
            if records_for_gen[a]['end_year'].strip() > '' and \
                records_for_gen[a]['end_year'].strip() > refined_possible_end_years[-1][0]:

                refined_possible_end_years.append([records_for_gen[a]['end_year'].strip(), 'end year from record'])

            #print(gen['GEN'], 'refined_possible_end_years (c)', refined_possible_end_years)

            records_for_gen[a]['refined_possible_end_years'] = refined_possible_end_years

        #print()
        #print(gen['GEN'], records_for_gen[0])

        #print()
        #print(gen['GEN'], records_for_gen[0]['refined_possible_end_years'])

        job = {'GEN': records_for_gen[0]['GEN'], 
                'name': records_for_gen[0]['name'],
                'office': records_for_gen[0]['office'],
                'occupation_type': records_for_gen[0]['occupation_type'],
                'occupation_status': records_for_gen[0]['occupation_status'],
                'section': records_for_gen[0]['section'],
                'division': records_for_gen[0]['division'],
                'department': records_for_gen[0]['department'],
                'location': records_for_gen[0]['location'], 
                'state': records_for_gen[0]['state'],
                'end_date': records_for_gen[0]['refined_possible_end_years'][-1][0],
                'details': [records_for_gen[0]]}

        if len(records_for_gen) > 1:
            for a in range(1, len(records_for_gen)):

                if records_for_gen[a]['GEN'] == job['GEN'] and \
                    records_for_gen[a]['location'] == job['location'] and \
                    records_for_gen[a]['state'] == job['state'] and \
                    records_for_gen[a]['refined_possible_end_years'][-1][0] <= job['end_date']:

                    job['details'].append(records_for_gen[a])

                else:

                    job['start_date'] = job['details'][0]['record_year']
                    job['office'] = job['details'][0]['office']

                    job['start_date'] = job['details'][0]['record_year']
                    job['office'] = job['details'][0]['office']
                    job['name'] = job['details'][0]['name']

                    all_jobs.append(job)  

                    job = {'GEN': records_for_gen[a]['GEN'], 
                            'name': records_for_gen[a]['name'],
                            'office': records_for_gen[a]['office'],
                            'occupation_type': records_for_gen[a]['occupation_type'],
                            'occupation_status': records_for_gen[a]['occupation_status'],
                            'section': records_for_gen[a]['section'],
                            'division': records_for_gen[a]['division'],
                            'department': records_for_gen[a]['department'],
                            'location': records_for_gen[a]['location'], 
                            'state': records_for_gen[a]['state'],
                            'end_date': records_for_gen[a]['refined_possible_end_years'][-1][0],
                            'details': [records_for_gen[a],]}

        job['start_date'] = job['details'][0]['record_year']
        job['office'] = job['details'][0]['office']
        job['name'] = job['details'][0]['name']

        all_jobs.append(job)

    print('len(all_jobs)', len(all_jobs))

    return all_jobs
    
def dump_to_json(data, file_name):
    
    f = open(file_name, 'w', encoding='utf-8')
    f.write(json.dumps(data, indent=4))
    f.close()


if __name__ == '__main__':

    INPUT_FOLDER = 'from_peter_102322/Dossier_Building/2022.10/'
    
    # ------------------------------------------------------------------
    
    csv_files = convert_input_to_csv(INPUT_FOLDER)

    df, original_records, standardized_records = \
            load_and_standardize_inputs(INPUT_FOLDER, csv_files)
    
    register_dates = get_register_dates(df)
    
    unique_gens = get_unique_gens(df)
    
    # ------------------------------------------------------------------
    
    df.to_csv('standardized_records.csv', index=False)
    
    dump_to_json(original_records, 'original_records.json')
    
    dump_to_json(register_dates, 'register_dates.json')
    
    dump_to_json(unique_gens, 'unique_gens.json')
    
    # ------------------------------------------------------------------

    SELECTED_GENS = None
    #SELECTED_GENS = ['GEN.000001', 'GEN.000002', 'GEN.000006', 'GEN.000007', 'GEN.000026']
    
    all_jobs = records_to_jobs(df, register_dates, unique_gens, SELECTED_GENS)
    
    # ------------------------------------------------------------------
    
    dump_to_json(all_jobs, 'all_jobs.json')
    
    # ------------------------------------------------------------------
    
    temp = []
    for a in all_jobs:
        temp.append([a['GEN'], a['start_date'], json.dumps(a)])

    all_jobs_by_gen = {}
    
    for a in sorted(temp):
        if a[0] not in all_jobs_by_gen:
            all_jobs_by_gen[a[0]] = []
        all_jobs_by_gen[a[0]].append(json.loads(a[2]))
    
    dump_to_json(all_jobs_by_gen, 'all_jobs_by_gen.json')
    
    print('Done!')
    
    # ------------------------------------------------------------------
    
