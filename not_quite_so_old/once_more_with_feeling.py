#!/Users/pentecost/opt/anaconda3/bin/python

import pandas

df = pandas.read_csv('from_peter_050622/TOITest1.csv').fillna('')
#df = pandas.read_csv('from_peter_050622/GEN.000005.csv').fillna('')

df = df.loc[df['Department'] == 'Treasury']

# ----------------------------------------------------------------------

source_date = df.groupby(['SourceCategory', 'Record Year']) \
                            .size() \
                            .reset_index() \
                            .rename(columns={0:'count'}).to_dict('records')

source_date = [a['Record Year'] for a in source_date if a['SourceCategory'] == 'Registers'] + [1834.0]

print('source_date', source_date)

# ----------------------------------------------------------------------

unique_gens = df.groupby(['GovernmentEmployeeNumber',]) \
                            .size() \
                            .reset_index() \
                            .rename(columns={0:'count'})

unique_gens = unique_gens.to_dict('records')

print('len(unique_gens)', len(unique_gens))
print('unique_gens[0]', unique_gens[0])

# ----------------------------------------------------------------------

def ok_to_backfill(years):
    
    result = True
    for y in years:
        if y[1].startswith('another'):
            result = False
    
    #print('ok_to_backfill', 'years', years, 'result', result)
    
    return result
    
def cull_years(years):
 
    result = []
    for y in years:
        result.append(y)
        if result[-1][1].startswith('another'):
            break
    return result

# ----------------------------------------------------------------------

all_jobs = []

for gen_n, gen in enumerate(unique_gens):
    
    records_for_gen = df.loc[df['GovernmentEmployeeNumber'] == gen['GovernmentEmployeeNumber']]

    records_for_gen = records_for_gen[['GovernmentEmployeeNumber', 'Full Name', 
                 'SourceCategory', 'Record Year', 
                 'Locality', 'State',
                 'Office', 'Occupation Type', 'Occupation Status', 
                 'End Year', 'End Year.1', 'RowNumber']].sort_values(by=['Record Year'])

    records_for_gen = records_for_gen.to_dict('records')
    
    print()
    print('gen_n', gen_n, 'gen', gen)

    for a in range(0, len(records_for_gen)):

        possible_end_years = []

        for b in range(a, len(records_for_gen)):

            match_type = ''

            if records_for_gen[a]['Locality'] == records_for_gen[b]['Locality'] and \
                records_for_gen[a]['State'] == records_for_gen[b]['State']:

                if records_for_gen[a]['Occupation Type'] == records_for_gen[b]['Occupation Type'] and \
                    records_for_gen[a]['Occupation Status'] == records_for_gen[b]['Occupation Status']:

                    if b == a:
                        match_type = 'starting date (a)'
                    else:
                        match_type = 'same office type, status, and place (a)'
                else:
                    a_office_words = records_for_gen[a]['Office'].lower().split(' ')
                    b_office_words = records_for_gen[b]['Office'].lower().split(' ')

                    office_word_tokens = a_office_words + b_office_words
                    office_word_types = set(a_office_words + b_office_words)

                    if float(len(office_word_tokens)) / float(len(office_word_types)) >= 1.5:
                        match_type = 'same office words (a)'
                    else:
                        break

            if match_type > '':
                possible_end_years.append([records_for_gen[b]['Record Year'], match_type])
                
        #
        #    DID ANYONE ELSE HAVE THE JOB?
        #
                
        refined_possible_end_years = []

        for b in range(0, len(possible_end_years)):

            refined_possible_end_years.append(possible_end_years[b])

            if b < len(possible_end_years) - 1:

                from_year = possible_end_years[b][0]
                to_year = possible_end_years[b + 1][0]

                records_for_others = df.loc[df['Record Year'] > from_year]
                records_for_others = records_for_others.loc[records_for_others['Record Year'] < to_year]

                records_for_others = records_for_others.loc[records_for_others['GovernmentEmployeeNumber'] != \
                                                 records_for_gen[a]['GovernmentEmployeeNumber']]

                records_for_others = records_for_others.loc[records_for_others['Locality'] == \
                                                 records_for_gen[a]['Locality']]
                records_for_others = records_for_others.loc[records_for_others['State'] == \
                                                 records_for_gen[a]['State']]
                records_for_others = records_for_others.loc[records_for_others['Occupation Type'] == \
                                                 records_for_gen[a]['Occupation Type']]
                records_for_others = records_for_others.loc[records_for_others['Occupation Status'] == \
                                                 records_for_gen[a]['Occupation Status']]

                records_for_others = records_for_others[['GovernmentEmployeeNumber', 'Full Name', 
                                                 'SourceCategory', 'Record Year', 
                                                 'Locality', 'State',
                                                 'Office', 'Occupation Type', 'Occupation Status', 
                                                 'End Year', 'End Year.1', 'RowNumber']] \
                                        .sort_values(by=['Record Year']) \
                                        .to_dict('records')
                
                if len(records_for_others) > 0 and \
                    refined_possible_end_years[-1][1].startswith('another') == False:

                    a_office_words = records_for_gen[a]['Office'].lower().split(' ')
                    rfo_office_words = records_for_others[0]['Office'].lower().split(' ')

                    office_word_tokens = a_office_words + rfo_office_words
                    office_word_types = set(a_office_words + rfo_office_words)

                    if float(len(office_word_tokens)) / float(len(office_word_types)) >= 1.5:

                        print()
                        print('records_for_gen', records_for_gen[a])
                        print()
                        print('records_for_others', records_for_others)
                        print()
                        
                        refined_possible_end_years.append([records_for_others[0]['Record Year'], 'another person same occupation etc (a) ' + records_for_others[0]['GovernmentEmployeeNumber']])
                else:

                    records_for_others = df.loc[df['Record Year'] > from_year]
                    records_for_others = records_for_others.loc[records_for_others['Record Year'] < to_year]

                    records_for_others = records_for_others.loc[records_for_others['GovernmentEmployeeNumber'] != \
                                                     records_for_gen[a]['GovernmentEmployeeNumber']]

                    records_for_others = records_for_others.loc[records_for_others['Locality'] == \
                                                     records_for_gen[a]['Locality']]
                    records_for_others = records_for_others.loc[records_for_others['State'] == \
                                                     records_for_gen[a]['State']]

                    records_for_others = records_for_others[['GovernmentEmployeeNumber', 'Full Name', 
                                                     'SourceCategory', 'Record Year', 
                                                     'Locality', 'State',
                                                     'Office', 'Occupation Type', 'Occupation Status', 
                                                     'End Year', 'End Year.1', 'RowNumber']] \
                                            .sort_values(by=['Record Year']) \
                                            .to_dict('records')
                    for rfo in records_for_others:

                        a_office_words = records_for_gen[a]['Office'].lower().split(' ')
                        rfo_office_words = rfo['Office'].lower().split(' ')

                        office_word_tokens = a_office_words + rfo_office_words
                        office_word_types = set(a_office_words + rfo_office_words)

                        if float(len(office_word_tokens)) / float(len(office_word_types)) >= 1.5:
                            refined_possible_end_years.append([
                                    records_for_others[0]['Record Year'], 
                                    'another person, same office (a) ' + rfo['GovernmentEmployeeNumber']])
                            break
                            
        #
        #    LOOK AHEAD IN THE REGISTERS
        #       

        if refined_possible_end_years[-1][0] == records_for_gen[a]['Record Year'] and \
            records_for_gen[a]['SourceCategory'] == 'Registers' and \
            int(records_for_gen[a]['Record Year']) >= 1816 and \
            refined_possible_end_years[-1][1].startswith('another') == False:
                
            next_date = None
            for d in range(0, len(source_date)):

                if source_date[d] == records_for_gen[a]['Record Year'] and \
                    d < len(source_date) - 1:

                    next_date = source_date[d + 1]
                    break

            if next_date != None and refined_possible_end_years[-1][1].startswith('another') == False:

                refined_possible_end_years.append([next_date, 'not in later register'])

                if a > 0:
                    for b in range(0, a):
                        
                        if records_for_gen[b]['refined_possible_end_years'][-1][0] == \
                                    refined_possible_end_years[-2][0]:

                            match_type = ''

                            if records_for_gen[a]['Locality'] == records_for_gen[b]['Locality'] and \
                                records_for_gen[a]['State'] == records_for_gen[b]['State']:

                                if records_for_gen[a]['Occupation Type'] == records_for_gen[b]['Occupation Type'] and \
                                    records_for_gen[a]['Occupation Status'] == records_for_gen[b]['Occupation Status']:

                                        if b == a:
                                            match_type = 'starting date (b)'
                                        else:
                                            match_type = 'same office type, status, and place (b)'
                                                
                                else:
                                    a_office_words = records_for_gen[a]['Office'].lower().split(' ')
                                    b_office_words = records_for_gen[b]['Office'].lower().split(' ')

                                    office_word_tokens = a_office_words + b_office_words
                                    office_word_types = set(a_office_words + b_office_words)

                                    if float(len(office_word_tokens)) / float(len(office_word_types)) >= 1.5:
                                        
                                        match_type = 'same office words (b)'
                                    else:
                                        break
                                        
                            if match_type > '':
                                
                                if ok_to_backfill(records_for_gen[b]['refined_possible_end_years']):

                                    records_for_gen[b]['refined_possible_end_years'] \
                                                    .append([refined_possible_end_years[-1][0],
                                                             refined_possible_end_years[-1][1] + ' BKFL'])
                            
        refined_possible_end_years = cull_years(refined_possible_end_years)

        records_for_gen[a]['refined_possible_end_years'] = refined_possible_end_years


    job = {'GovernmentEmployeeNumber': records_for_gen[0]['GovernmentEmployeeNumber'], 
            'Locality': records_for_gen[0]['Locality'], 
            'State': records_for_gen[0]['State'],
            'end_date': records_for_gen[0]['refined_possible_end_years'][-1][0],
            'details': [records_for_gen[0]]}

    if len(records_for_gen) > 1:
        for a in range(1, len(records_for_gen)):

            if records_for_gen[a]['GovernmentEmployeeNumber'] == job['GovernmentEmployeeNumber'] and \
                records_for_gen[a]['Locality'] == job['Locality'] and \
                records_for_gen[a]['State'] == job['State'] and \
                records_for_gen[a]['refined_possible_end_years'][-1][0] == job['end_date']:

                job['details'].append(records_for_gen[a])

            else:

                job['start_date'] = job['details'][0]['Record Year']
                job['Office'] = job['details'][0]['Office']

                job['start_date'] = job['details'][0]['Record Year']
                job['Office'] = job['details'][0]['Office']
                job['Full Name'] = job['details'][0]['Full Name']

                all_jobs.append(job)  

                job = {'GovernmentEmployeeNumber': records_for_gen[a]['GovernmentEmployeeNumber'], 
                        'Locality': records_for_gen[a]['Locality'], 
                        'State': records_for_gen[a]['State'],
                        'end_date': records_for_gen[a]['refined_possible_end_years'][-1][0],
                        'details': [records_for_gen[a],]}

    job['start_date'] = job['details'][0]['Record Year']
    job['Office'] = job['details'][0]['Office']
    job['Full Name'] = job['details'][0]['Full Name']

    all_jobs.append(job)

print('len(all_jobs)', len(all_jobs))

# ----------------------------------------------------------------------


import json

f = open('all_jobs.json', 'w', encoding='utf-8')
f.write(json.dumps(all_jobs))
f.close()

print('ok')

