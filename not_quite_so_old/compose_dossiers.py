#!/home/spenteco/anaconda2/envs/py3/bin/python
    
import json
from mako.template import Template
import pandas as pd

def make_dossiers(INPUT_FILE, DOSSIERS_FOLDER, N_LIMIT_TO_PROCESS):

    f = open(INPUT_FILE, 'r', encoding='utf-8')
    
    all_jobs = json.load(f)
    
    f.close()
    
    temp = []
    for a in all_jobs:
        temp.append([a['GEN'], a['start_date'], json.dumps(a)])

    all_jobs_by_gen = {}
    
    for a in sorted(temp):
        if a[0] not in all_jobs_by_gen:
            all_jobs_by_gen[a[0]] = []
        all_jobs_by_gen[a[0]].append(json.loads(a[2]))
        
    n_processed = 0    
        
    mytemplate = Template(filename='gen_template.html')
    
    for gen, data in all_jobs_by_gen.items():
    
        n_processed += 1
        if n_processed > N_LIMIT_TO_PROCESS:
            break
            
        n_jobs = len(data)
        n_records = 0
        for d in data:
            n_records += len(d['details'])

        result = mytemplate.render(gen=gen, n_jobs=n_jobs, n_records=n_records, all_jobs=data)

        f = open(DOSSIERS_FOLDER + gen + '.html', 'w', encoding='utf-8')
        f.write(result)
        f.close()

def make_index_page(INPUT_FILE, INDEX_FOLDER, N_LIMIT_TO_PROCESS):

    f = open(INPUT_FILE, 'r', encoding='utf-8')
    
    all_jobs = json.load(f)
    
    f.close()
    
    temp = []
    for a in all_jobs:
        temp.append([a['GEN'], a['start_date'], json.dumps(a)])

    all_jobs_by_gen = {}
    
    for a in sorted(temp):
        if a[0] not in all_jobs_by_gen:
            all_jobs_by_gen[a[0]] = []
        all_jobs_by_gen[a[0]].append(json.loads(a[2]))
        
    n_processed = 0    
    
    index_data = []
    
    for gen, data in all_jobs_by_gen.items():
    
        n_processed += 1
        if n_processed > N_LIMIT_TO_PROCESS:
            break
            
        n_jobs = len(data)
        n_records = 0
        for d in data:
            n_records += len(d['details'])
            
        index_data.append({'status': 'not_approved',
                            'gen': gen,
                            'name': data[0]['name'],
                            'n_jobs': n_jobs,
                            'n_records': n_records})

    
    df = pd.DataFrame(index_data)
    
    df.to_csv(INDEX_FOLDER + 'index_data.csv', index=False)
    
if __name__ == '__main__':

    INPUT_FILE = 'all_jobs.json'
    DOSSIERS_FOLDER = 'web_review_dossiers/dossiers/'
    INDEX_FOLDER = 'web_review_dossiers/data/'
    N_LIMIT_TO_PROCESS = 999999
    
    make_dossiers(INPUT_FILE, DOSSIERS_FOLDER, N_LIMIT_TO_PROCESS)
    
    make_index_page(INPUT_FILE, INDEX_FOLDER, N_LIMIT_TO_PROCESS)
    
