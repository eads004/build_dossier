#!/home/spenteco/anaconda2/envs/py3/bin/python

import glob, subprocess, json, re
from datetime import datetime
import pandas as pd
from mako.template import Template

def patch_df(raw_df):
    
    data = raw_df.to_dict('records')
    
    for a in range(0, len(data)):
        
        for column in ['GEN', 'name', 'office', 'occupation_status', 'occupation_type', 
                        'state', 'location', 'start_year', 'end_year', 'section', 
                        'division', 'department', 'source_category', 
                        'record_year', 'input_file', 
                        'concurrent?', 'confirmed?', 'okay?']:
                            
            data[a][column] = str(data[a][column])
            if data[a][column] == '0.0':
                data[a][column] = ''
            else:
                data[a][column] = data[a][column].strip()
            
    df = pd.DataFrame(data)
    
    return df

def ok_to_backfill(years):
    
    result = True
    for y in years:
        if y[1].startswith('another'):
            result = False
    
    return result
    
def cull_years(years):
 
    result = []
    for y in years:
        result.append(y)
        if result[-1][1].startswith('another'):
            break
    return result

def do_locations_match(a, b):
    
    result = False
    
    if a['location'] == b['location'] and a['state'] == b['state']:
        
        result = True
    
    return result

def records_to_jobs(df, register_dates, unique_gens, SELECTED_GENS):
    
    all_jobs = []

    df_list = df.to_dict('records')

    for gen_n, gen in enumerate(unique_gens):

        if SELECTED_GENS != None and gen['GEN'] not in SELECTED_GENS:
            continue
        
        records_for_gen = df.loc[df['GEN'] == gen['GEN']]

        records_for_gen = records_for_gen[['GEN', 'name', 
                     'source_category', 'record_year', 
                     'location', 'state',
                     'office', 'occupation_type', 'occupation_status', 
                     'section', 'division', 'department',
                     'start_year', 'end_year', 'input_file', 'original_record',
                     'concurrent?', 'confirmed?', 'okay?']].sort_values(by=['record_year'])

        #test_records = records_for_gen[['GEN', 'record_year', 
        #             'start_year', 'end_year']].sort_values(by=['record_year'])
                     
        #print(test_records)
        #print()
        
        records_for_gen = records_for_gen.to_dict('records')
        
        if len(records_for_gen) == 0:
            continue
        
        is_okay = []
        
        jobs_for_gen = []
        grouped_jobs_for_gen = None
        
        for ra, r in enumerate(records_for_gen):

            #print(r['GEN'], r['source_category'])
                
            if r['occupation_type'] == 'Leadership' or \
                r['occupation_status'] == 'Leadership':
                records_for_gen[ra]['occupation_type'] = r['office']
            
            if r['okay?'] > '':
                is_okay.append(r['okay?'])
            
            if len(jobs_for_gen) == 0:
                jobs_for_gen.append([r])
            else:
                    
                occupation_type = r['occupation_type']
                occupation_status = r['occupation_status']
                    
                if r['concurrent?'].strip() > '':
                    
                    prior_matched = False
                    
                    z = len(jobs_for_gen)
                    
                    while z > 0:
                        
                        z = z -1
                    
                        prior_occupation_type = jobs_for_gen[z][-1]['occupation_type']
                        prior_occupation_status = jobs_for_gen[z][-1]['occupation_status']
                        
                        if prior_occupation_type == 'Leadership' or \
                            prior_occupation_status == 'Leadership':
                            prior_occupation_type = jobs_for_gen[z][-1]['office']
                        
                        if occupation_type == prior_occupation_type and \
                            occupation_status == prior_occupation_status and \
                            do_locations_match(jobs_for_gen[z][-1], r) == True:
                                
                            jobs_for_gen[z].append(r)
                            
                            prior_matched = True
                            
                            break
                            
                    if prior_matched == False:
                        jobs_for_gen.append([r])
                
                else:
                    
                    was_appended = False
                    j_index = len(jobs_for_gen)
                    
                    #print()
                    
                    while True:
                        
                        j_index = j_index - 1
                        if j_index < 0:
                            break
                            
                        j = jobs_for_gen[j_index]
                        
                        prior_occupation_type = j[-1]['occupation_type']
                        prior_occupation_status = j[-1]['occupation_status']
                    
                        if prior_occupation_type == 'Leadership' or \
                            prior_occupation_status == 'Leadership':
                            prior_occupation_type = j[-1]['office']
                    
                        #print(j_index,
                        #        occupation_type, occupation_status,
                        #        r['location'], r['state'],
                        #        '>>',
                        #        prior_occupation_type, prior_occupation_status,
                        #        j[-1]['location'], j[-1]['state'],
                        #        do_locations_match(j[-1], r))
                                
                        if occupation_type == prior_occupation_type and \
                            occupation_status == prior_occupation_status and \
                            do_locations_match(j[-1], r) == True:
                            
                            jobs_for_gen[j_index].append(r)
                            was_appended = True
                            
                            #print('\tAPPENDED')
                            
                            break
                        
                    if was_appended == False:
                        jobs_for_gen.append([r])
                        #print('\tNEW')
                    
        if len(is_okay) > 0:
            is_okay = '; '.join(sorted(list(set(is_okay))))
        else:
            is_okay = ''
                
        grouped_jobs_for_gen = {'GEN': gen['GEN'], 
                                'name': records_for_gen[0]['name'], 
                                'okay?': is_okay, 
                                'jobs': []}

        for j in jobs_for_gen:
            
            all_start_dates = []
            all_end_dates = []
            
            occupation_type = ' MISSING TYPE?'
            occupation_status = 'MISSING STATUS?'
            
            for r in j:

                #print()
                #print(r['GEN'], r['source_category'], r['record_year'], r['start_year'], r['end_year'])
                
                if r['occupation_type'].strip() > '':
                    occupation_type = r['occupation_type']
                if r['occupation_status'].strip() > '':
                    occupation_status = r['occupation_status']
                
                if str(r['record_year']).strip() > '' and str(r['record_year']).strip() != '0.0':
                    all_start_dates.append([str(r['record_year']), r['source_category']])

                if str(r['start_year']).strip() > '' and str(r['start_year']).strip() != '0.0':
                    all_start_dates.append([str(r['start_year']), r['source_category']])

                if str(r['end_year']).strip() > '' and str(r['end_year']).strip() != '0.0':
                    all_end_dates.append([str(r['end_year']), r['source_category']])
                    
            all_start_dates.sort()
            all_end_dates.sort()
            
            #print()
            #print(gen['GEN'], 'all_start_dates', all_start_dates)
            #print(gen['GEN'], 'all_end_dates', all_end_dates)
            
            early_start_date = ''
            late_start_date = ''
            end_date = ''
            
            end_date_message = ''

            if len(all_start_dates) > 0:

                early_start_date = all_start_dates[0][0].split('.')[0]
                late_start_date = all_start_dates[-1][0].split('.')[0]
            
                if len(all_end_dates) > 0 and \
                    all_end_dates[-1][0].split('.')[0] >= late_start_date:
                    end_date = all_end_dates[-1][0].split('.')[0]
                else:
                    for a in range(0, len(register_dates)):
                        if register_dates[a].split('.')[0] > late_start_date.split('.')[0]:
                            end_date = register_dates[a].split('.')[0]
                            end_date_message = 'end_date inferred from register'
                            break
                            
            if early_start_date > '':
                
                print_location = j[0]['location'].strip()
                if print_location == 'Capital':
                    print_location = ''
                    
                print_state = j[0]['state'].strip()
                if print_state == 'Capital':
                    print_state = ''
                    
                print_place = str(print_location)
                
                if str(print_state).strip() > '':
                    if print_place > '':
                        print_place = print_place + ', ' + str(print_state).strip()
                    else:
                        print_place = str(print_state).strip()

                grouped_jobs_for_gen['jobs'].append({'office': ', '.join([occupation_type, occupation_status]),
                                                        'print_place': print_place,
                                                        'start_date': early_start_date,
                                                        'end_date': end_date,
                                                        'end_date_message': end_date_message,
                                                        'job_details': []})
                for r in j:

                    grouped_jobs_for_gen['jobs'][-1]['job_details'].append({
                                                'record_year': r['record_year'], 
                                                'start_year': r['start_year'], 
                                                'end_year': r['end_year'], 
                                                'office': r['office'], 
                                                'occupation_type': r['occupation_type'], 
                                                'occupation_status': r['occupation_status'], 
                                                'location': r['location'], 
                                                'state': r['state'], 
                                                'confirmed?': r['confirmed?'], 
                                                'source_category': r['source_category']})

    
        if grouped_jobs_for_gen != None:
            all_jobs.append(grouped_jobs_for_gen)
            
    for a, person in enumerate(all_jobs):
        
        job_dates = []
        
        for job in person['jobs']:
        
            job_dates.append((job['start_date'], job['end_date']))
            
        job_dates = sorted(list(set(job_dates)))
        
        if len(job_dates) > 0:
        
            date_ranges = [{'start_date': job_dates[0][0], 'end_date': job_dates[0][1]}]
            
            for job in job_dates[1:]:
            
                job_matched_date_range = False
            
                for b in range(0, len(date_ranges)):
                
                    if job[1] < date_ranges[b]['start_date'] or job[0] > date_ranges[b]['end_date']:
                        pass
                    else:
                    
                        if job[0] < date_ranges[b]['start_date']:
                            date_ranges[b]['start_date'] = job[0]
                    
                        if job[1] > date_ranges[b]['end_date']:
                            date_ranges[b]['end_date'] = job[1]
                        
                        job_matched_date_range = True
                        break
                        
                if job_matched_date_range == False:
                
                    date_ranges.append({'start_date': job[0], 'end_date': job[1]})
                
            years_in_office = 0
            for d in date_ranges:
                try:
                    years_in_office += 1 + (int(d['end_date']) - int(d['start_date']))
                except ValueError:
                    years_in_office = 'ValueError computing years_in_office'
                    break
                    
            all_jobs[a]['terms_in_office'] = date_ranges[0]['start_date'] + '-' + date_ranges[0]['end_date']
            for d in date_ranges[1:]:
                all_jobs[a]['terms_in_office'] += '; ' + d['start_date'] + '-' + d['end_date']
            
            all_jobs[a]['n_terms_in_office'] = len(date_ranges)
            all_jobs[a]['years_in_office'] = years_in_office
            
        else:
            all_jobs[a]['terms_in_office'] = 'No jobs? terms_in_office'
            all_jobs[a]['n_terms_in_office'] = 'No jobs? n_terms_in_office'
            all_jobs[a]['years_in_office'] = 'No jobs? years_in_office'
            
    return all_jobs
    
def dump_to_json(data, file_name):
    
    f = open(file_name, 'w', encoding='utf-8')
    f.write(json.dumps(data, indent=4))
    f.close()


if __name__ == '__main__':
    
    raw_df = pd.read_csv('standardized_records.csv').fillna(0.0)
    
    df = patch_df(raw_df)
    
    original_records = json.load(open('original_records.json', 'r', encoding='utf-8'))
    
    register_dates = json.load(open('register_dates.json', 'r', encoding='utf-8'))
    
    unique_gens = json.load(open('unique_gens.json', 'r', encoding='utf-8'))
    
    # ------------------------------------------------------------------

    SELECTED_GENS = None
    #SELECTED_GENS = ['GEN.000022', 'GEN.000024', 'GEN.000192', 'GEN.000360', 'GEN.000589', 'GEN.000849', 'GEN.000875', 'GEN.000893', 'GEN.001141', 'GEN.001226', 'GEN.001253', 'GEN.001368', 'GEN.001565', 'GEN.001882', 'GEN.001926', 'GEN.002012', 'GEN.002160', 'GEN.002265', 'GEN.002532', 'GEN.002658', 'GEN.003060', 'GEN.003148', 'GEN.003562', 'GEN.003570', 'GEN.004260', 'GEN.004307', 'GEN.004378', 'GEN.004533', 'GEN.004742', 'GEN.004934', 'GEN.005072', 'GEN.005113', 'GEN.005280', 'GEN.005371', 'GEN.005617', 'GEN.005665', 'GEN.005876', 'GEN.006651', 'GEN.006684', 'GEN.006772', 'GEN.006953', 'GEN.007230', 'GEN.007328', 'GEN.007860', 'GEN.007932', 'GEN.008143', 'GEN.008457', 'GEN.008540', 'GEN.008708', 'GEN.008943', 'GEN.009372', 'GEN.009389', 'GEN.009600' ]
    #SELECTED_GENS = ['GEN.001141', 'GEN.004378']
    
    all_jobs = records_to_jobs(df, register_dates, unique_gens, SELECTED_GENS)
    
    # ------------------------------------------------------------------
    
    dump_to_json(all_jobs, 'all_jobs.json')
    
    now = datetime.now().strftime("%Y/%m/%d, %H:%M:%S")
    file_now = re.sub('[^0-9]', '_', now)
    
    template = Template(filename='report_template.html')
    
    for a in range(0, 6):
        
        split_all_jobs = all_jobs[a * 2650: (a * 2650) + 2650]
    
        html = template.render(now=now, all_jobs=split_all_jobs)
        
        f = open('kastor_proof_reports_no_subloc_010323/test_010323.' + str(a) + '.html', 'w', encoding='utf-8')
        f.write(html)
        f.close()
    
    # ------------------------------------------------------------------
