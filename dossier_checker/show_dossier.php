<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Show Dossier!</title>
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/show_dossier.css">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="js/show_dossier.js"></script>
    </head>
    <body>

<?php

$GLOBALS['db'] = new SQLite3('/home/spenteco/3/build_dossier/sqlite3_data_102322/jobs_data.sqlite3');


if (isset($_GET['status'])) {

    $sql = 'UPDATE gens SET status = "' . $_GET['status'] . '" WHERE GEN = "' . $_GET['GEN'] . '";';

    $db->exec($sql);
}

$status = '';

$sql = 'SELECT status FROM gens WHERE GEN = "' . $_GET['GEN'] . '";';

$results = $GLOBALS['db']->query($sql);

while ($row = $results->fetchArray()) {
    $status = $row['status'];
    break;
}

$sql = 'SELECT * FROM jobs WHERE GEN = "' . $_GET['GEN'] . '" ORDER BY start_date;';

$results = $GLOBALS['db']->query($sql);

$jobs = Array();

while ($row = $results->fetchArray()) {
    array_push($jobs, $row);
}

$name = $jobs[0]['name'];

$sql = 'SELECT * FROM details WHERE GEN = "' . $_GET['GEN'] . '" ORDER BY start_year;';

$results = $GLOBALS['db']->query($sql);

$details = Array();

while ($row = $results->fetchArray()) {
    array_push($details, $row);
}

$sql = 'SELECT * FROM end_years WHERE GEN = "' . $_GET['GEN'] . '" ORDER BY CAST(job_n AS INTEGER), CAST(detail_n AS INTEGER), CAST(sequence AS INTEGER);';

$results = $GLOBALS['db']->query($sql);

$end_years = Array();

while ($row = $results->fetchArray()) {
    array_push($end_years, $row);
}

?>

    <div class="heading">

        <span class="heading_text" id="gen_heading"><?php echo $_GET['GEN']; ?></span>
        <span class="heading_text"><?php echo $name; ?></span>

        <span style="display:none;" id="status_stash"><?php echo $status; ?></span>

        <input type="radio" name="status" value="not_checked">
        <label>not_checked</label>
        <input type="radio" name="status" value="AUTO">
        <label>AUTO</label>
        <input type="radio" name="status" value="OK">
        <label>OK</label>
        <input type="radio" name="status" value="BAD_CODE">
        <label>BAD_CODE</label>
        <input type="radio" name="status" value="BAD_DATA">
        <label>BAD_DATA</label>

    </div>

    <div class="section_title">Aggregated Jobs</div>

    <table class="">
        <thead>
            <tr>
                <th>office</th>
                <th>occupation_type</th>
                <th>occupation_status</th>
                <th>department</th>
                <th>division</th>
                <th>section</th>
                <th>location</th>
                <th>state</th>
                <th>start_date</th>
                <th>end_date</th>
                <th>job_n</th>
            </tr>
        </thead>
        <tbody>
<?php

foreach ($jobs as $job) {

    $section_parts = explode(' ', $row['section']);
    $section_short_name = implode(' ', array_slice($section_parts, 0, 3));

    echo '<tr>' . 
            '<td>' . $job['office'] . '</td>' . 
            '<td>' . $job['occupation_type'] . '</td>' . 
            '<td>' . $job['occupation_status'] . '</td>' . 
            '<td>' . $job['department'] . '</td>' . 
            '<td>' . $job['division'] . '</td>' . 
            '<td>' . $section_short_name . '</td>' . 
            '<td>' . $job['location'] . '</td>' . 
            '<td>' . $job['state'] . '</td>' . 
            '<td>' . $job['start_date'] . '</td>' . 
            '<td>' . $job['end_date'] . '</td>' . 
            '<td>' . $job['job_n'] . '</td>' . 
        '</tr>';
}

?>
        </tbody>
    </table>

    <div class="section_title">Standardized Detail Records</div>

    <table class="">
        <thead>
            <tr>
                <th>name</th>
                <th>office</th>
                <th>occupation_type</th>
                <th>occupation_status</th>
                <th>department</th>
                <th>division</th>
                <th>section</th>
                <th>location</th>
                <th>state</th>
                <th>source_category</th>
                <th>record_year</th>
                <th>start_year</th>
                <th>end_year</th>
                <th>job_n</th>
                <th>detail_n</th>
            </tr>
        </thead>
        <tbody>
<?php

foreach ($details as $detail) {

    $section_parts = explode(' ', $row['section']);
    $section_short_name = implode(' ', array_slice($section_parts, 0, 3));

    echo '<tr>' . 
            '<td>' . $detail['name'] . '</td>' . 
            '<td>' . $detail['office'] . '</td>' . 
            '<td>' . $detail['occupation_type'] . '</td>' . 
            '<td>' . $detail['occupation_status'] . '</td>' . 
            '<td>' . $detail['department'] . '</td>' . 
            '<td>' . $detail['division'] . '</td>' . 
            '<td>' . $detail['section'] . '</td>' . 
            '<td>' . $detail['location'] . '</td>' . 
            '<td>' . $detail['state'] . '</td>' . 
            '<td>' . $detail['source_category'] . '</td>' . 
            '<td>' . $detail['record_year'] . '</td>' . 
            '<td>' . $detail['start_year'] . '</td>' . 
            '<td>' . $detail['end_year'] . '</td>' . 
            '<td>' . $detail['job_n'] . '</td>' . 
            '<td>' . $detail['detail_n'] . '</td>' . 
        '</tr>';
}

?>
        </tbody>
    </table>

    <div class="section_title">End_year Reason Codes</div>

    <table class="">
        <thead>
            <tr>
                <th>job_n</th>
                <th>detail_n</th>
                <th>sequence</th>
                <th>year</th>
                <th>reason</th>
            </tr>
        </thead>
        <tbody>
<?php

foreach ($end_years as $end_year) {

    echo '<tr>' . 
            '<td>' . $end_year['job_n'] . '</td>' . 
            '<td>' . $end_year['detail_n'] . '</td>' . 
            '<td>' . $end_year['sequence'] . '</td>' . 
            '<td>' . $end_year['year'] . '</td>' . 
            '<td>' . $end_year['reason'] . '</td>' . 
        '</tr>';
}

?>
        </tbody>
    </table>


    </body>
</html>
