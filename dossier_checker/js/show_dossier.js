
$(
    function() {

        var status = $('#status_stash').html();

        $('input[name="status"]').each(
            function() {
                if ($(this).val() == status) {
                    $(this).prop('checked', true);
                }
            }
        );

        $('input[name="status"]').change(
            function() {

                var url = window.location.origin + 
                                    '/dossier_checker/update_status.php' + 
                                    '?GEN=' + $('#gen_heading').html() + '&status=' + $(this).val();

                $.get(url, function(data) {
                    console.log('calling', url, data);
                });
            }
        );

    }
);
