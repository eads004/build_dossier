<!DOCTYPE html>
<html>
<?php

    $GLOBALS['db'] = new SQLite3('/home/spenteco/3/build_dossier/sqlite3_data_102322/jobs_data.sqlite3');

    function get_jobs_facet($value_column, $predicates) {

        $predicate_string = implode(' AND ', $predicates);
        if ($predicate_string > '') {
            $predicate_string = 'AND ' . $predicate_string;
        }

        $results = $GLOBALS['db']->query('SELECT ' . $value_column . ' as v, count(*) AS n FROM jobs a, gens b WHERE a.GEN = b.GEN ' . $predicate_string . ' GROUP BY 1 ORDER BY 2 DESC;');
        
        echo '<div class="facet_line"><span class="facet_value"><b>' . $value_column . '</span></b></div>';

        echo '<div class="facet_container">';

        while ($row = $results->fetchArray()) {
            echo '<div class="facet_line"><span class="facet_value">' . $row['v'] . '</span><span class="facet_n">' . $row['n'] . '</span></div>';

        }

        echo '</div>';
    }

?>
    <head>
        <meta charset="UTF-8">
        <title>Dossier Checker!</title>
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/checker.css">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="js/tableManager.js"></script>
        <script src="js/checker.js"></script>
    </head>
    <body>
    
        <div id="page_header">
            <span id="page_title">Dossier Checker!</span>
            <span id="error_message"></span>
        </div>

        <div id="left_pane">
        
        <div id="search_controls">
            <form action="index.php">
                <div class="control_line">
                    <label class="search_label">Status:</label><input type="text" id="status_input" name="status_input" value="<?php echo $_GET['status_input'];?>"/>
                </div>
                <div class="control_line">
                    <label class="search_label">Person name:</label><input type="text" id="person_name_input" name="person_name_input" value="<?php echo $_GET['person_name_input'];?>"/>
                </div>
                <div class="control_line">
                    <label class="search_label">Office:</label><input type="text" id="office_input" name="office_input" value="<?php echo $_GET['office_input'];?>"/>
                </div>
                <div class="control_line">
                    <label class="search_label">Department:</label><input type="text" id="department_input" name="department_input" value="<?php echo $_GET['department_input'];?>"/>
                </div>
                <div class="control_line">
                    <label class="search_label">Division:</label><input type="text" id="division_input" name="division_input" value="<?php echo $_GET['division_input'];?>"/>
                </div>
                <div class="control_line">
                    <label class="search_label">Section:</label><input type="text" id="section_input" name="section_input" value="<?php echo $_GET['section_input'];?>"/>
                </div>
                <div class="control_line">
                    <label class="search_label">State:</label><input type="text" id="state_input" name="state_input" value="<?php echo $_GET['state_input'];?>"/>
                </div>
                <div class="control_line">
                    <label class="search_label">Location:</label><input type="text" id="location_input" name="location_input" value="<?php echo $_GET['location_input'];?>"/>
                </div>
                <div class="control_line">
                    <label class="search_label">Start Year:</label><input type="text" id="start_year_input" name="start_year_input" value="<?php echo $_GET['start_year_input'];?>"/>
                </div>
                <div class="control_line">
                    <label class="search_label">End Year:</label><input type="text" id="end_year_input" name="end_year_input" value="<?php echo $_GET['end_year_input'];?>"/>
                </div>
                <div class="control_line">
                    <label class="search_label"> </label><input type="submit" value="Filter!"/>
                </div>
            </form>
        </div>

        <div class="facets">
<?php

$predicates = Array();

if(isset($_GET['status_input']) && $_GET['status_input'] != '') {
    array_push($predicates, 'status LIKE "%' . $_GET['status_input'] . '%"');
}
if(isset($_GET['person_name_input']) && $_GET['person_name_input'] != '') {
    array_push($predicates, 'name LIKE "%' . $_GET['person_name_input'] . '%"');
}
if(isset($_GET['office_input']) && $_GET['office_input'] != '') {
    array_push($predicates, 'office LIKE "%' . $_GET['office_input'] . '%"');
}
if(isset($_GET['department_input']) && $_GET['department_input'] != '') {
    array_push($predicates, 'department LIKE "%' . $_GET['department_input'] . '%"');
}
if(isset($_GET['division_input']) && $_GET['division_input'] != '') {
    array_push($predicates, 'division LIKE "%' . $_GET['division_input'] . '%"');
}
if(isset($_GET['section_input']) && $_GET['section_input'] != '') {
    array_push($predicates, 'section LIKE "%' . $_GET['section_input'] . '%"');
}
if(isset($_GET['state_input']) && $_GET['state_input'] != '') {
    array_push($predicates, 'state LIKE "%' . $_GET['state_input'] . '%"');
}
if(isset($_GET['location_input']) && $_GET['location_input'] != '') {
    array_push($predicates, 'location LIKE "%' . $_GET['location_input'] . '%"');
}
if(isset($_GET['start_year_input']) && $_GET['start_year_input'] != '') {
    array_push($predicates, 'start_date >= "' . $_GET['start_year_input'] . '"');
}
if(isset($_GET['end_year_input']) && $_GET['end_year_input'] != '') {
    array_push($predicates, 'end_date <= "' . $_GET['end_year_input'] . '"');
}

get_jobs_facet('status', $predicates);
get_jobs_facet('office', $predicates);
get_jobs_facet('department', $predicates);
get_jobs_facet('division', $predicates);
get_jobs_facet('section', $predicates);
get_jobs_facet('state', $predicates);
get_jobs_facet('location', $predicates);
?>
        </div>

        </div>

        <div id="right_pane">

        <div class="results">

<?php

echo '<table class="tablemanager">';
echo '<thead>';
echo '<tr>'. 
        '<th>status</th>' . 
        '<th>GEN</th>' . 
        '<th>name</th>' .
        '<th>office</th>' .
        '<th>department</th>' .
        '<th>division</th>' .
        '<th>section</th>' .
        '<th>location</th>' .
        '<th>state</th>' . 
        '<th>start_date</th>' .
        '<th>end_date</th>' .
    '</tr>';
echo '</thead>';

echo '<tbody>';

$predicate_string = implode(' AND ', $predicates);
if ($predicate_string > '') {
    $predicate_string = 'AND ' . $predicate_string;
}

$sql_statement = 'SELECT * FROM jobs a, gens b WHERE a.GEN = b.GEN ' . $predicate_string . ' ORDER BY GEN, start_date;';        
$results = $db->query($sql_statement);

$n_jobs = 0;
$results_truncated = false;

$last_gen = 'STARTING GEN';

while ($row = $results->fetchArray()) {

    $section_parts = explode(' ', $row['section']);
    $section_short_name = implode(' ', array_slice($section_parts, 0, 3));

    if ($last_gen != $row['GEN']) {

        echo '<tr>'. 
                '<td>' . $row['status'] . '</td>' . 
                '<td><a href="show_dossier.php?GEN=' . $row['GEN'] . '">' . $row['GEN'] . '</a></td>' . 
                '<td>' . $row['name'] . '</td>' .
                '<td>' . $row['office'] . '</td>' .
                '<td>' . $row['department'] . '</td>' .
                '<td>' . $row['division'] . '</td>' .
                '<td>' . $section_short_name . '</td>' .
                '<td>' . $row['location'] . '</td>' .
                '<td>' . $row['state'] . '</td>' . 
                '<td>' . $row['start_date'] . '</td>' .
                '<td>' . $row['end_date'] . '</td>' .
            '</tr>';

        $last_gen = $row['GEN'];

        $n_jobs += 1;
    
    }

    if ($n_jobs >= 500) {
        $results_truncated = true;
        break;
    }
}

echo '</tbody>';

echo '</table>';
?>
        </div>
        
        </div>

        <div id="error_message_stash">
<?php
    if ($results_truncated == true) {
        echo 'Warning: the table of details is truncated!  Try a filter . . . ';
    }
?>
        </div>

    </body>
</html>
