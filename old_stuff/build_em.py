#!/home/spenteco/anaconda2/envs/py3/bin/python

import pandas as pd
from mako.template import Template

def load_data_for_each_person():

    data_by_person = {}

    for file_name in ['from_peter_022322/Army_Chronology.csv',
                        'from_peter_022322/Diplomats.csv',
                        'from_peter_022322/Army_Origins.csv',
                        'from_peter_022322/Judicial.csv',
                        'from_peter_032322/Combined2022.03.csv',
                        'from_peter_022322/Navy.csv',
                        'from_peter_032322/Cabinent2022.03.csv',
                        'from_peter_022322/Postmasters.csv'
        ]:

    #for file_name in ['test_data/Army_Chronology.csv',
    #                    'test_data/Diplomats.csv',
    #                    'test_data/Army_Origins.csv',
    #                    'test_data/Judicial.csv',
    #                    'test_data/Combined2022.03.csv',
    #                    'test_data/Navy.csv',
    #                    'test_data/Cabinent2022.03.csv',
    #                    'test_data/Postmasters.csv'
    #    ]:


        original_df = pd.read_csv(file_name, dtype='unicode')
        original_df = original_df.fillna('')
        original_df = original_df.to_dict('records')
        
        df = []
        
        for original_row in original_df:
            
            row = {'spreadsheet': file_name.split('/')[-1].split('.')[0]}
            
            for k, v in original_row.items():
                if k.startswith('Unnamed: ') == False:
                    row[k] = v
            df.append(row)
            
        for row in df:

            person_key = None
            if 'GEN' in row:
                person_key = row['GEN']
            elif 'GovernmentEmployeeNumber' in row:
                person_key = row['GovernmentEmployeeNumber']
                
            if person_key not in data_by_person:
                data_by_person[person_key] = []
            data_by_person[person_key].append(row)
        
        print(file_name, len(df))
        
    print(len(data_by_person))

    return data_by_person

def get_names(records):
        
    names = []
    
    for r in records:
        
        try:
        
            this_name = r['Last Name'].strip() + ', ' + r['First Name'].strip()
            if r['Middle Name'].strip() > '':
                this_name = this_name + ' ' + r['Middle Name'].strip()
            if r['Suffix'].strip() > '':
                this_name = this_name + ' ' + r['Suffix'].strip()
            names.append(this_name)
            
        except KeyError:
            print()
            print('KEYERROR A', r)
            break

    names = sorted(list(set(names)))

    return names

def get_sources(records):
        
    sources = []
    
    for r in records:
        
        try:
            
            this_source = [r['SourceCategory']]
            
            if 'Volume' in r and str(r['Volume']).strip() > '':
                if len(str(r['Volume']).split('.')[0]) == 4:
                    this_source.append(str(r['Volume']).split('.')[0])
                    
                else:
                    this_source.append(str(r['Volume']).split('.')[0])
            else:
                this_source.append('')
                
            if 'Page' in r and str(r['Page']).strip() > '':
                this_source.append(str(r['Page']).replace('*', '').rjust(6, '0'))
            else:
                this_source.append('')

            if 'Image' in r and r['Image'].strip() > '':
                this_source.append(r['Image'])
            else:
                this_source.append('')
                
            if this_source[0] == 'Heitman' and this_source[2] == '':
                pass
            else:
                sources.append(tuple(this_source))
            
        except KeyError:
            print()
            print('KEYERROR B', r)
            break
        
    sources = sorted(list(set(sources)))
                
    return sources

def get_office_details(row):
    
    this_office = {}
    
    if 'Office' in row:
        if row['Office'].strip() > '':
            this_office['Office'] = row['Office'].strip()
        else:
            return this_office

    if 'Section' in row and row['Section'].strip() > '':
        this_office['Section'] = row['Section'].strip()

    #if 'Division' in row and row['Division'].strip() > '':
    #    this_office = this_office + ', ' + row['Division'].strip()

    #if 'Department' in row and row['Department'].strip() > '':
    #    this_office = this_office + ', ' + row['Department'].strip()

    if 'Locality' in row and row['Locality'].strip() > '':
        if row['Locality'].strip() != '** Unknown **':
            this_office['Locality'] = row['Locality'].strip()

    #if 'State Section' in row and row['State Section'].strip() > '':
    #    this_office = this_office + ', ' + row['State Section'].strip()
        
    if 'State' in row and row['State'].strip() > '':
        this_office['State'] = row['State'].strip()
        
    #if 'Region' in row and row['Region'].strip() > '':
    #    this_office = this_office + ', ' + row['Region'].strip()
        
    if 'Native Nation' in row and row['Native Nation'].strip() > '':
        this_office['Native Nation'] = row['Native Nation'].strip()
        
    # ----------------------------------------------------------------
        
    if 'Record Year' in row and str(row['Record Year']).strip() > '':
        this_office['start_year'] = row['Record Year'].strip()
        
    if 'Start Year' in row and str(row['Start Year']).strip() > '':
        this_office['start_year'] = row['Start Year'].strip()

    if 'End Year' in row and str(row['End Year']).strip() > '':
        this_office['end_year'] = row['End Year'].strip()

    if 'Start Year.1' in row and str(row['Start Year.1']).strip() > '':
        this_office['start_year'] = row['Start Year.1'].strip()

    if 'End Year.1' in row and str(row['End Year.1']).strip() > '':
        this_office['end_year'] = row['End Year.1'].strip()
        
    # ----------------------------------------------------------------
        
    if 'Departure Reason' in row and str(row['Departure Reason']).strip() > '':
        this_office['Departure Reason'] = str(row['Departure Reason']).strip()
        
    # ----------------------------------------------------------------
        
    return this_office

def reduce_offices(offices):

    data_by_office = {}

    for o in offices:

        office_key = [o['Office']]

        if 'Locality' in o and o['Locality'].strip() != 'Capital':
            office_key.append(o['Locality'])
        else:
            office_key.append('')

        if 'State' in o and o['State'].strip() != 'Capital':
            office_key.append(o['State'])
        else:
            office_key.append('')

        if 'Section' in o:
            office_key.append(o['Section'])
        else:
            office_key.append('')

        office_key = tuple(office_key)

        if office_key not in data_by_office:
            data_by_office[office_key] = []

        data_by_office[office_key].append(o)

    reduced_offices = []

    for k, v in data_by_office.items():

        start_years = []
        end_years = []
        departure_reasons = []

        for o in v:
            if 'start_year' in o:
                start_years.append(o['start_year'])
            if 'end_year' in o:
                end_years.append(o['end_year'])
            if 'Departure Reason' in o and o['Departure Reason'].strip() > '':
                departure_reasons.append(o['Departure Reason'])

        start_years.sort()
        end_years.sort(reverse=True)
        departure_reasons = sorted(list(set(departure_reasons)))        

        final_start_year = ''
        if len(start_years) > 0:
            final_start_year = start_years[0]
    
        final_end_year = ''
        if len(end_years) > 0 and end_years[0] != '0':
            final_end_year = end_years[0]

        output_line = [final_start_year, final_end_year, k[0], k[1], k[2], k[3], '; '.join(departure_reasons)]

        reduced_offices.append(output_line)
    
    return reduced_offices

def get_offices(records):
        
    offices = []
    
    for r in records:
        
        this_office = []
        
        if r['spreadsheet'].startswith('Army_Chronology'):
            this_office = get_office_details(r)
        
        if r['spreadsheet'].startswith('Army_Origins'):
            pass
        
        if r['spreadsheet'].startswith('Diplomats'):
            this_office = get_office_details(r)
        
        if r['spreadsheet'].startswith('Judicial'):
            this_office = get_office_details(r)
        
        if r['spreadsheet'].startswith('Cabinet'):
            this_office = get_office_details(r)
        
        if r['spreadsheet'].startswith('Navy'):
            this_office = get_office_details(r)
        
        if r['spreadsheet'].startswith('Combined'):
            this_office = get_office_details(r)
        
        if r['spreadsheet'].startswith('Postmasters'):
            this_office = get_office_details(r)
        
        if len(this_office) > 0:
            offices.append(this_office)

    final_offices = sorted(reduce_offices(offices))

    return final_offices

def get_salaries(records, gen):
        
    salaries = []
    
    for r in records:
        
        salary = ''
        pay_period = ''
        start_year = ''
        end_year = ''
        
        if 'Salary' in r and str(r['Salary']).strip() > '':
            salary = str(r['Salary']).strip()
        
        if 'Pay Period' in r and str(r['Pay Period']).strip() > '':
            pay_period = str(r['Pay Period']).strip()
            
        if 'Record Year' in r and str(r['Record Year']).strip() > '':
            start_year = str(r['Record Year']).strip().split('.')[0]
            
        if 'Start Year' in r and str(r['Start Year']).strip() > '':
            start_year = str(r['Start Year']).strip().split('.')[0]

        if 'End Year' in r and str(r['End Year']).strip() > '':
            end_year = str(r['End Year']).strip().split('.')[0]

        if 'Start Year.1' in r and str(r['Start Year.1']).strip() > '':
            start_year = str(r['Start Year.1']).strip().split('.')[0]

        if 'End Year.1' in r and str(r['End Year.1']).strip() > '':
            end_year = str(r['End Year.1']).strip().split('.')[0]
            
        if salary > '':
            salaries.append((start_year, end_year, salary, pay_period))

    salaries.sort()

    return salaries

def get_birth_death_data(records):
        
    birth_death_data = {'birth_year': [], 'death_year': [], 'birth_place': [], 'death_place': []}
    
    for r in records:
        
        this_location = ''
        
        if 'Birth Locality' in r and str(r['Birth Locality']).strip() > '':
            this_location = str(r['Birth Locality']).strip()
        
        if 'Birth State' in r and str(r['Birth State']).strip() > '':
            if this_location > '':
                this_location = this_location + ', ' + str(r['Birth State']).strip()
            else:
                this_location = str(r['Birth State']).strip()
        
        if 'Birth Country' in r and str(r['Birth Country']).strip() > '':
            if this_location > '':
                this_location = this_location + ', ' + str(r['Birth Country']).strip()
            else:
                this_location = str(r['Birth Country']).strip()
                
        if this_location > '':
            birth_death_data['birth_place'].append(this_location)
        
        this_location = ''
        
        if 'Death Locality' in r and str(r['Death Locality']).strip() > '':
            this_location = str(r['Death Locality']).strip()
        
        if 'Death State' in r and str(r['Death State']).strip() > '':
            if this_location > '':
                this_location = this_location + ', ' + str(r['Death State']).strip()
            else:
                this_location = str(r['Death State']).strip()
        
        if 'Death Country' in r and str(r['Death Country']).strip() > '':
            if this_location > '':
                this_location = this_location + ', ' + str(r['Death Country']).strip()
            else:
                this_location = str(r['Death Country']).strip()
                
        if this_location > '':
            birth_death_data['Death_place'].append(this_location)
        
        if 'Birth Year' in r and str(r['Birth Year']).strip() > '':
            birth_death_data['birth_year'].append(str(r['Birth Year']).strip().split('.')[0])
        
        if 'Death Year' in r and str(r['Death Year']).strip() > '':
            birth_death_data['death_year'].append(str(r['Death Year']).strip().split('.')[0])
    
    return birth_death_data

def get_military_biography(records):

    birth_state = ''
    birth_country = ''
    appointed_state = ''
    appointed_country = ''

    for r in records:
        if r['spreadsheet'] == 'Army_Origins':

            birth_state = r['Birth State']
            birth_country = r['Birth Country']
            appointed_state = r['Appointed State']
            appointed_country = r['Appointed Country']

    return {'birth_state': birth_state, 
                'birth_country': birth_country,
                'appointed_state': appointed_state, 
                'appointed_country': appointed_country}
            
def accumulate_reporting_data(data_by_person):

    accumulated_data_data_by_person = {}

    for gen, records in data_by_person.items():

        accumulated_data_data_by_person[gen] = {}
        
        accumulated_data_data_by_person[gen]['names'] = get_names(records)
        
        accumulated_data_data_by_person[gen]['sources'] = get_sources(records)
        
        accumulated_data_data_by_person[gen]['offices'] = get_offices(records)
        
        accumulated_data_data_by_person[gen]['salaries'] = get_salaries(records, gen)
                
        accumulated_data_data_by_person[gen]['birth_death_data'] = get_birth_death_data(records)
                
        accumulated_data_data_by_person[gen]['military_biography'] = get_military_biography(records)

    return accumulated_data_data_by_person

def output_dossiers(accumulated_data_data_by_person, data_by_person, MIN_N_OFFICES_TO_OUTPUT=-1):

    mytemplate = Template(filename='template.html')

    n = 0

    for gen, data in accumulated_data_data_by_person.items():
        
        if gen != None:

            if len(data['offices']) < MIN_N_OFFICES_TO_OUTPUT:
                continue

            if data['offices'][0][0] > '':
                int_year = -1
                try:
                    int_year = int(data['offices'][0][0])
                except ValueError:
                    pass
                if int_year > 1832:
                    continue
                
            birth_line = ''

            if len(data['birth_death_data']['birth_year']) > 0:
                birth_line = str(data['birth_death_data']['birth_year']).split('.')[0]

            if len(data['birth_death_data']['birth_place']) > 0:

                if birth_line == '':
                     birth_line ='; '.join(sorted(list(set(data['birth_death_data']['birth_place']))))
                else:
                     birth_line = birth_line + ' - ' + \
                        '; '.join(sorted(list(set(data['birth_death_data']['birth_place']))))
                
            death_line = ''
            if len(data['birth_death_data']['death_year']) > 0:
                death_line = str(data['birth_death_data']['death_year']).split('.')[0]
            if len(data['birth_death_data']['death_place']) > 0:
                if death_line == '':
                     death_line ='; '.join(sorted(list(set(data['birth_death_data']['death_place']))))
                else:
                     death_line = death_line + ' - ' + \
                        '; '.join(sorted(list(set(data['birth_death_data']['death_place']))))

            military_bio_line = ''

            if '; '.join(sorted(list(set(data['birth_death_data']['birth_place'])))) == '':

                if data['military_biography']['birth_state'] > '':
                    military_bio_line = 'Born: ' + data['military_biography']['birth_state']

                if data['military_biography']['birth_country'] > '':
                    if military_bio_line > '':
                        military_bio_line = military_bio_line + ', ' + data['military_biography']['birth_country']

            if data['military_biography']['appointed_state'] > '':
                if military_bio_line > '':
                    military_bio_line = military_bio_line + '; '
                military_bio_line = military_bio_line + '<b>Place of military appointment:</b> ' + data['military_biography']['appointed_state']

            if data['military_biography']['appointed_country'] > '':
                if military_bio_line > '':
                    military_bio_line = military_bio_line + ', ' + data['military_biography']['appointed_country']

            raw_data = {}
            for r in data_by_person[gen]:
                if r['spreadsheet'] not in raw_data:
                    raw_data[r['spreadsheet']] = []
                raw_data[r['spreadsheet']].append(r)

            result = mytemplate.render(gen = gen, 
                                        data = data, 
                                        birth_line = birth_line, 
                                        death_line = death_line,
                                        military_bio_line = military_bio_line,
                                        raw_data = raw_data)

            f = open('dossier_files/' + gen + '.html', 'w', encoding='utf-8')
            f.write(result)
            f.close()

            #n += 1
            #if n > 10:
            #    break

    print('ok!')

if __name__ == "__main__":

    data_by_person = load_data_for_each_person()
    accumulated_data_data_by_person = accumulate_reporting_data(data_by_person)
    output_dossiers(accumulated_data_data_by_person, data_by_person, MIN_N_OFFICES_TO_OUTPUT=10)
